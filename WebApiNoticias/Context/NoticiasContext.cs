﻿using Microsoft.EntityFrameworkCore;
using WebApiNoticias.Models;

namespace WebApiNoticias.Context
{
    public class NoticiasContext : DbContext
    {
        public NoticiasContext(DbContextOptions opciones) : base(opciones)
        {

        }
         public DbSet<Noticias> Noticias { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Autor>().ToTable("Autor");
            modelBuilder.Entity<Noticias>().ToTable("Noticias");
        }


    }
}
