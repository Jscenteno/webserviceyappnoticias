﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiNoticias.Models;
using WebApiNoticias.Services;

namespace WebApiNoticias.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NoticiasController : ControllerBase
    {
        private readonly NoticiasService _noticiasService;

        public NoticiasController(NoticiasService noticiasService)
        {
            _noticiasService = noticiasService;
        }

        
        [HttpGet]     
        public IActionResult VerNoticias()
        {
            var resultado = _noticiasService.VerListadoTodasLasNoticias();
            return Ok(resultado);
        }

        [HttpGet("{NoticiaID}")]    
        public IActionResult NoticiaPorID(int NoticiaID)
        {
            return Ok(_noticiasService.ObtenerPorID(NoticiaID));
        }

        [HttpPost]      
        public IActionResult Agregar([FromBody] Noticias NoticiaAgregar)
        {
            if (_noticiasService.Agregar(NoticiaAgregar))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPut]    
        public IActionResult Editar([FromBody] Noticias NoticiaEditar)
        {
            if (_noticiasService.Editar(NoticiaEditar))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        [Route("{noticiaID}")]
        public IActionResult Eliminar(int noticiaID)
        {
            if (_noticiasService.Eliminar(noticiaID))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }

        }




    }
}
