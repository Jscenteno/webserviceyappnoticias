﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiNoticias.Context;
using WebApiNoticias.Models;

namespace WebApiNoticias.Services
{
    public class NoticiasService
    {

        public readonly NoticiasContext _noticiasContext;
        public NoticiasService(NoticiasContext noticiasContext)
        {
            _noticiasContext = noticiasContext;
        }


        public List<Noticias> VerListadoTodasLasNoticias()
        {
            var Noticiasr = _noticiasContext.Noticias.OrderByDescending(x => x.NoticiaID).ToList();
            return Noticiasr;
        }

        public Noticias ObtenerPorID(int NoticiaID)
        {
            try
            {
                var Noticia = _noticiasContext.Noticias.Where(x => x.NoticiaID == NoticiaID).FirstOrDefault();

                return Noticia;
            }
            catch (Exception error)
            {
                return new Noticias();
            }
        }

        public bool Agregar(Noticias noticia)
        {
            try
            {
                _noticiasContext.Noticias.Add(noticia);
                _noticiasContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool Editar(Noticias noticia)
        {
            try
            {
                var noticias = _noticiasContext.Noticias.FirstOrDefault(x => x.NoticiaID == noticia.NoticiaID);
                noticias.Titulo = noticia.Titulo;
                noticias.Descripcion = noticia.Descripcion;
                noticias.Contenido = noticia.Contenido;
                noticias.Autor = noticia.Autor;
                _noticiasContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Eliminar(int NoticiaID)
        {
            try
            {
                var noticiaEliminar = _noticiasContext.Noticias.FirstOrDefault(x => x.NoticiaID == NoticiaID);
                _noticiasContext.Noticias.Remove(noticiaEliminar);
                _noticiasContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

      
    }
}