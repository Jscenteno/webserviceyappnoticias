﻿using AppNoticia.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppNoticia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNoticia : ContentPage
    {
        private NoticiaManager _manager;
        public AddNoticia(NoticiaManager manager)
        {
            InitializeComponent();
            _manager = manager;
        }

        public async void OnSaveNoticia(object sender, EventArgs e)
        {
            await _manager.Add(txtTitulo.Text,txtDescripcion.Text,txtContenido.Text,txtAutor.Text);
           
        }
    }
}