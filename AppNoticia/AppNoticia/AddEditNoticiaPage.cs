﻿using AppNoticia.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace AppNoticia
{
    public class AddEditNoticiaPage : ContentPage
    {
        readonly Noticia existingNoticia;
        readonly EntryCell tituloCell, descripcionCell, contenidoCell, autorCell;
        readonly IList<Noticia> noticias;
        readonly NoticiaManager manager;

        public AddEditNoticiaPage(NoticiaManager manager, IList<Noticia> noticias, Noticia existingNoticia = null)
        {
            this.manager = manager;
            this.noticias = noticias;
            this.existingNoticia = existingNoticia;

            var tableView = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot(existingNoticia != null ? "Editar Noticia" : "New Noticia") {
                    new TableSection("Detalle") {
                       
                        (tituloCell = new EntryCell {
                            Label = "Título:",
                            Placeholder = "Agregar título",
                            Text = (existingNoticia != null) ? existingNoticia.Titulo : null,
                        }),
                        (descripcionCell = new EntryCell {
                            Label = "Descripción:",
                            Placeholder = "Agregar descripción",
                            Text = (existingNoticia != null) ? existingNoticia.Descripcion : null,
                        }),
                        (contenidoCell = new EntryCell {
                            Label = "Contenido:",
                            Placeholder = "Agregar contenido",
                            Text = (existingNoticia != null) ? existingNoticia.Contenido : null,
                        }),
                        (autorCell = new EntryCell {
                            Label = "Autor:",
                            Placeholder = "Agregar autor",
                            Text = (existingNoticia != null) ? existingNoticia.Autor : null,
                        }),
                    },
                }
            };

            Button button = new Button()
            {
                BackgroundColor = existingNoticia != null ? Color.Blue : Color.Green,
                TextColor = Color.White,
                Text = existingNoticia != null ? "Actualizar" : "Agregar",
                CornerRadius = 0,
            };
            button.Clicked += OnDismiss;

            Content = new StackLayout
            {
                Spacing = 0,
                Children = { tableView, button },
            };
        }

        async void OnDismiss(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.IsEnabled = false;
            this.IsBusy = true;
            try
            {
                string titulo = tituloCell.Text;
                string descripcion = descripcionCell.Text;
                string contenido  = contenidoCell.Text;
                string autor = autorCell.Text;

                if (string.IsNullOrWhiteSpace(titulo)
                    || string.IsNullOrWhiteSpace(descripcion)
                    || string.IsNullOrWhiteSpace(contenido)
                    || string.IsNullOrWhiteSpace(autor)
                    )
                {
                    this.IsBusy = false;
                    await this.DisplayAlert("falta información",
                        "Debe ingresar valores en todo los campos",
                        "OK");
                }
                else
                {
                    if (existingNoticia != null)
                    {
                        existingNoticia.Titulo = titulo;
                        existingNoticia.Descripcion = descripcion;
                        existingNoticia.Contenido = contenido;
                        existingNoticia.Autor = autor;

                        await manager.Update(existingNoticia);
                        int pos = noticias.IndexOf(existingNoticia);
                        noticias.RemoveAt(pos);
                        noticias.Insert(pos, existingNoticia);
                    }
                    else
                    {
                        Noticia noticia = await manager.Add(titulo, descripcion, contenido,autor);
                    }

                    await Navigation.PopModalAsync();
                }

            }
            finally
            {
                this.IsBusy = false;
                button.IsEnabled = true;
            }
        }
    }
}