﻿using AppNoticia.data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNoticia
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        readonly IList<Noticia> noticias = new ObservableCollection<Noticia>();
        readonly NoticiaManager _manager = new NoticiaManager();
        public MainPage()
        {
            BindingContext = noticias;
            InitializeComponent();
            Refresh();
        }
        async void OnRefresh(object sender, EventArgs e)
        {
            Refresh();
        }
        async  void Refresh()
        {
            var tareasCollection = await _manager.GetAll();
            foreach (Noticia noticia in tareasCollection)
            {
                if (noticias.All( n => n.NoticiaID != noticia.NoticiaID))
                {
                    noticias.Add(noticia);
                }
            }
        }

        async private void OnAddNoticia(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddNoticia(_manager));          
            await Navigation.PopAsync();
        }

        async void OnEditNoticia(object sender, ItemTappedEventArgs e)
        {
            await Navigation.PushModalAsync(
                new AddEditNoticiaPage(_manager, noticias, (Noticia)e.Item));
            await Navigation.PopAsync();
        }

        async void OnDeleteNoticia(object sender, EventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            Noticia noticia = item.CommandParameter as Noticia;
            if (noticia != null)
            {
                if (await this.DisplayAlert("Eliminar la noticia?",
                    "Estas seguero de eliminar esta noticia '"
                        + noticia.Titulo + "'?", "Sí", "Cancelar") == true)
                {
                    await _manager.Delete(noticia.NoticiaID);
                    noticias.Remove(noticia);
                }
            }
            await Navigation.PopAsync();
        }
    }
}
