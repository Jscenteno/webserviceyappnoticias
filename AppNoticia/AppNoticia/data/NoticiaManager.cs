﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppNoticia.data
{
    public class NoticiaManager
    {

        const string Url = "http://192.168.254.131/noticias/";


        /// <summary>
        /// Método para obtener toda las noticias
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Noticia>> GetAll()
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(Url);
            return JsonConvert.DeserializeObject<IEnumerable<Noticia>>(result);
        }

        /// <summary>
        /// Métdo para agregar una noticia 
        /// </summary>
        /// <param name="titulo"></param>
        /// <param name="descripcion"></param>
        /// <param name="contenido"></param>
        /// <param name="Autor"></param>
        /// <returns></returns>
        public async Task<Noticia> Add(string titulo, string descripcion, string contenido, string Autor)
        {
            Noticia noticia = new Noticia()
            {
                Titulo = titulo,
                Descripcion = descripcion,
                Contenido = contenido,
                Autor = Autor
            };

            HttpClient client = new HttpClient();

            var response = await client.PostAsync(Url,
                new StringContent(
                    JsonConvert.SerializeObject(noticia),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Noticia>(
                await response.Content.ReadAsStringAsync());
        }

        public async Task Update(Noticia noticia)
        {
            HttpClient client = new HttpClient();
            await client.PutAsync(Url,
                new StringContent(
                    JsonConvert.SerializeObject(noticia),
                    Encoding.UTF8, "application/json"));
        }

        public async Task Delete(int NoticiaID)
        {
            HttpClient client = new HttpClient();
            await client.DeleteAsync(Url + NoticiaID);
        }
    }

}
