﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppNoticia.data
{
    public class Noticia
    {
        public int NoticiaID { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Contenido { get; set; }
        public string Autor { get; set; }


    }
}
